#include "hal_wifi_manager.h"

void wifi_manager(void)
{
    WiFiManager wifiManager;
    wifiManager.resetSettings();
    wifiManager.autoConnect(WIFI_NAME);
    Serial.println("connected :)");
}