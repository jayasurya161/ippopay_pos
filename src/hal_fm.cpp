#include "hal_fm.h"

TEA5767 radio;    // Create an instance of Class for Si4703 Chip

uint8_t test1;
byte test2;

void fm_init(void)
{
    Serial.println("Radio..");
    delay(100);

    radio.init();
    radio.debugEnable();

    radio.setBandFrequency(FIX_BAND, FIX_STATION);
    radio.setVolume(2);
    radio.setMono(false);
}

void fm_player(void)
{
    char s[12];
    radio.formatFrequency(s, sizeof(s));
    Serial.print("Station: ");
    Serial.println(s);

    Serial.print("Radio: ");
    radio.debugAudioInfo();

    Serial.print("Audio: ");
    radio.debugAudioInfo();

    //Enable this delay if needed
    //delay(3000);
}