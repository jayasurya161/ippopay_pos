#include "hal_mqtt.h"

// Replace the next variables with your SSID/Password combination
const char *ssid = "uniRoom";
const char *password = "unisem123@";
const char *sub_topic = "test/sub";
const char *pub_topic = "test/pub";

int val = 0;

// Add your MQTT Broker IP address, example:
// const char* mqtt_server = "192.168.1.144";
const char *mqtt_server = "3.139.114.119";


WiFiClient espClient;
PubSubClient client(espClient);
//mPayload MqttPayload;

long lastMsg = 0;
char msg[50];
int value = 0;

//mainFlags Flags;

void mqttInit()
{
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

}

void setup_wifi(String ssidPay, String passwordPay)
{
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  //Serial.println(ssid);

  if(ssidPay.length ()> 3 and passwordPay.length() > 5)
  {
    wifiCred.ssidValue = ssidPay.c_str();
    wifiCred.passwordValue = passwordPay.c_str();
    Serial.println("EEPROM SSID, PASSWORD");
    Serial.print("SSID: ");
    Serial.println(ssidPay);
    Serial.print("Password: ");
    Serial.println(passwordPay);
    WiFi.begin(wifiCred.ssidValue, wifiCred.passwordValue);
  }

  else
  {
    WiFi.begin(ssid, password);
  }

  while (WiFi.status() != WL_CONNECTED)
  {
    static uint8_t count;
    count++;
    delay(500);
    Serial.print(".");
    if(count>10)
      break;
  }

  if(WiFi.status() == WL_CONNECTED)
    wifiCred.wifiStatus = true;
  else
    wifiCred.wifiStatus = false;

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char *topic, byte *message, unsigned int length)
{
  Serial.print("Message arrived on topic: ");
  Serial.println(topic);
 
  String messageTemp;

  for (int i = 0; i < length; i++)
  {
    //Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  Flags.payloadRecFlag = 1;
  Flags.payload = (String)messageTemp;
}

void reconnect()
{
  // Loop until we're reconnected
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ippopay_two", "ippopay_two", "ippopay_two"))
    {
      Serial.println("connected");
      // Subscribe
      client.subscribe("test/sub");
      String temp = Details.DeviceId+"/details";
      client.subscribe(temp.c_str());
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}



void mqttConnectionCheck()
{
  if (!client.connected())
  {
    reconnect();
  }
  client.loop();
}

void jsonPrase(String payload)
{
    DynamicJsonDocument doc(1024);
    deserializeJson(doc, payload);
    JsonObject obj = doc.as<JsonObject>();
    /*
    MqttPayload.fmFreqency = obj[String("fm")][String("frequency")];
    MqttPayload.volume = obj[String("fm")][String("volume")];
    MqttPayload.mute = obj[String("fm")][String("mute")];
    MqttPayload.state = obj[String("fm")][String("state")];

   // MqttPayload.PaymentMode = obj[String("payment")][String("mode")];
    MqttPayload.PaymentMode = obj[String("payment")][String("mode")];
    MqttPayload.amount = obj[String("payment")][String("amount")];
    */

    MqttPayload.msgType = obj[String("msgType")];

    switch (MqttPayload.msgType)
    {
    case REG_MODE:
      MqttPayload.RegRequest = obj[String("Registration_Request")];
      MqttPayload.accountID = obj["account_id"].as<String>();
      Details.status = true;
      Details.Assigned_ID = MqttPayload.accountID;
      Serial.print("Regustration Request: ");
      Serial.println(MqttPayload.RegRequest);
      Serial.print("Account IDL: ");
      Serial.println(Details.Assigned_ID);
      break;
    
    case TRANSACTION_MODE:
      MqttPayload.PaymentMode = obj[String("Transaction")];
      MqttPayload.amount = obj[String("Amount")];

      if(1 == MqttPayload.PaymentMode)
      {
        Serial.print("Amount Credited: ");
        Serial.println(MqttPayload.amount);        
      }

      else if(!MqttPayload.PaymentMode)
      {
        Serial.print("Amount Debited: ");
        Serial.println(MqttPayload.amount);
      }

      break;
    
    case FM_MODE:
      break;

    case SPEAKER_CTRL_MODE:
      break;

    default:
      break;
    }
}


void mqtt_publish(String topic, char payload[512])
{
  if(!client.connected())
    reconnect();

  client.loop();

  client.publish(topic.c_str(), payload);
}