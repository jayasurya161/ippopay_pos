#include "hal_eeprom.h"

//WiFiStruct wifiCred; 

void eeprom_wifiCred_check()
{
    EEPROM.begin(512);
    int lenCheck = 0, StringLen = 0;

    lenCheck = EEPROM.read(EEPROM_WIFI_VALUE_LEN_ADDR);

    String con = EEPROM.readString(EEPROM_WIFI_VALE_ADDR);

    StringLen = con.length();

    if(lenCheck == StringLen)
    {
        Serial.println("Good to Go");
        int in = con.indexOf(",");
        wifiCred.ssidString = con.substring(0, in);
        wifiCred.passwordString = con.substring(in + 1);
        Serial.print("SSID: ");
        Serial.println(con.substring(0, in));
        Serial.print("Password: ");
        Serial.println(con.substring(in + 1));
    }

    else
    {
        Serial.println("Im taking the default value from the core if you don't mind!!");
        wifiCred.ssidString = "\0";
        wifiCred.passwordString = "\0";
    }
    EEPROM.end();
}

void eeprom_device_reg_check()
{
    EEPROM.begin(512);

    int checkAck = EEPROM.read(EEPROM_ACC_REG_ACK_ADDR);

    if(ACC_REF_ACK == checkAck)
    {
        Serial.println("This device is already assigned to: ");
        Details.Assigned_ID = EEPROM.readString(EEPROM_ACC_REF_VALUE_ADDR);
        Details.status = true;
        Details.DeviceId = WiFi.macAddress();
        Serial.print("Device Mac Address: ");
        Serial.println(Details.DeviceId);
        Serial.print("Assigned Device ID: ");
        Serial.println(Details.Assigned_ID);
    }
    else
    {
        Serial.println("This device is not assigne yet");
        Details.status = false;
        Details.DeviceId = WiFi.macAddress();
        Serial.print("Device Mac Address: ");
        Serial.println(Details.DeviceId);
    }

    EEPROM.end();
}

void eeprom_device_reg_update()
{
    EEPROM.begin(512);

    if(1 == MqttPayload.RegRequest)
    {
        MqttPayload.RegRequest = 0;
        EEPROM.write(EEPROM_ACC_REG_ACK_ADDR, ACC_REF_ACK);
        EEPROM.writeString(EEPROM_ACC_REF_VALUE_ADDR, Details.Assigned_ID);
        EEPROM.commit();
        Details.status = true;
    }

    EEPROM.end();
}
