#include "main.h"

mPayload MqttPayload;
WiFiStruct wifiCred;
DeviceDetails Details;

void sendResponse_toServer()
{
  if(wifiCred.wifiStatus)
  {
    eeprom_device_reg_check();
    mqttInit();
    mqttConnectionCheck();
    if(false == Details.status)
    {
      String temp = "newDevice/details";
      Serial.print("Topic: ");
      Serial.println(temp);
      StaticJsonDocument<200> doc;
      doc["MAC_ID"] = Details.DeviceId;
      doc["Registration_Request"] = Details.status;
      doc["Update"] = "NULL";
      char jsonBuffer[512];
      serializeJson(doc, jsonBuffer);
      Serial.println(jsonBuffer);

      mqtt_publish(temp, jsonBuffer);
      Serial.println("JSON Send");
    }
  }
}


void sendDevice_Details()
{
  String temp = Details.DeviceId+"/Details";
  Serial.print("Topic: ");
  Serial.println(temp);
  StaticJsonDocument<200> doc;

  doc["MAC_ID"] = Details.DeviceId;
  doc["Battery"] = 56;
  doc["Power"] = 1;
  doc["Vol"] = 60;
  doc["SSID"] = wifiCred.ssidString;
  doc["Password"] = wifiCred.passwordString;
  doc["Log"] = "Nothing to say";
  doc["Ack"] = 1;

  char jsonBuffer[512];
  serializeJson(doc, jsonBuffer);
  Serial.print("Details Payload: ");
  Serial.println(jsonBuffer);

  mqtt_publish(temp, jsonBuffer);
  Serial.println("Device details has sent to server");
}

void setup()
{
  Serial.begin(115200);

  //playerInit();
  gpio_init();
  eeprom_wifiCred_check();
  delay(1000);
  setup_wifi(wifiCred.ssidString, wifiCred.passwordString);
  sendResponse_toServer();
  //fm_init();

}

void loop()
{
  static uint8_t count1;
  count1++;

  if(count1 >= 60)
  {
    Serial.println("Sending device details to the server");
    count1 = 0;
    if(wifiCred.wifiStatus)
    {
      Serial.println("WiFi is connected");
      sendDevice_Details();
    }
    else
    {
      Serial.println("WiFi is not Connected, can't able to send the packet.\nConfigure the WiFi with the internet.");
    }
  }

  //Serial.println("Hello World");
  if(0 == ioRead(RESET_BUTTON))
  {
    static uint8_t tempCount;
    while(!ioRead(RESET_BUTTON))
    {
      tempCount++;
      delay(1000);
    }
    if(tempCount > 5 and tempCount < 10)
    {
      Serial.println("WiFi Manager Enabled !!");
      wifi_manager();

      Serial.println("WiFi Configured..\nChecking and updating the EEPROM");

      sendResponse_toServer();
    }
    
  }
 
  if(wifiCred.wifiStatus)
  {
    mqttConnectionCheck();
    if(1 == Flags.payloadRecFlag)
    {
      Serial.print("Packet Received: ");
      Serial.println(Flags.payload);

      /*JSON Parse */
      jsonPrase(Flags.payload);

      if(1 == MqttPayload.RegRequest)
      {
        Serial.println("EEPROM Reg Request update");
        eeprom_device_reg_update();
      }

      Flags.payload = "";
      Flags.payloadRecFlag = 0;
    }
  }
   delay(1000);
}