#include "hal_gpio.h"

void gpio_init(void)
{
    pinMode(RESET_BUTTON, INPUT_PULLUP);
    //attachInterrupt(RESET_BUTTON, isrTrig, RISING);
}

uint8_t ioRead(uint8_t pin)
{
    return digitalRead(pin);
}