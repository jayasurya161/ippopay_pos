#ifndef HAL_PLAYER_H
#define HAL_PLAYER_H
#include "main.h"
#include "DFRobotDFPlayerMini.h"

extern void printDetail(uint8_t type, int value);
extern void playerInit();
extern void packetCheck(String payload);

#endif