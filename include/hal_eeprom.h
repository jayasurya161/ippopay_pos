#ifndef HAL_EEPROM_H
#define HAL_EEPROM_H
#include "main.h"
#include <EEPROM.h>


#define EEPROM_ACC_REG_ACK_ADDR                 0x50
#define EEPROM_ACC_REF_VALUE_ADDR               0x52
#define EEPROM_WIFI_VALE_ADDR                   0x04
#define EEPROM_WIFI_VALUE_LEN_ADDR              0x02

#define ACC_REF_ACK                             12

extern void eeprom_wifiCred_check();
extern void eeprom_device_reg_check();
extern void eeprom_device_reg_update();

#endif