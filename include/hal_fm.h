#ifndef HAL_FM_H
#define HAL_FM_H
#include "main.h"
#include <Wire.h>
#include <radio.h>
#include <TEA5767.h>

/// The band that will be tuned by this sketch is FM.
#define FIX_BAND RADIO_BAND_FM

/// The station that will be tuned by this sketch is 89.30 MHz.
#define FIX_STATION 9110



extern void fm_init(void);
extern void fm_play(void);

#endif