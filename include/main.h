#ifndef MAIN_H
#define MAIN_H
//1621
#include <Arduino.h>
#include "hal_player.h"
#include "hal_mqtt.h"
#include "hal_fm.h"
#include "hal_gpio.h"
#include "hal_wifi_manager.h"
#include "hal_eeprom.h"

typedef struct flagsV
{
    String payload;
    int payloadRecFlag;
    long lastMsg = 0;
    char msg[50];
}mainFlags;


typedef struct mqttPayload
{
    float fmFreqency;
    uint8_t volume;
    uint8_t mute;
    uint8_t state;

    uint8_t PaymentMode;
    float amount;

    uint8_t msgType;
    uint8_t RegRequest;
    String accountID;
}mPayload;


typedef struct wifiS
{
    String ssidString;
    String passwordString;
    const char *ssidValue;
    const char *passwordValue;
    bool wifiStatus;
}WiFiStruct;


typedef struct deviceDetails
{
    String DeviceId;
    String Assigned_ID;
    bool status;
}DeviceDetails;


extern void sendResponse_toServer();
extern void sendDevice_Details();


extern mainFlags Flags;
extern mPayload MqttPayload;
extern WiFiStruct wifiCred;
extern DeviceDetails Details;

#endif
