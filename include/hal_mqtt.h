#ifndef HAL_MQTT_H
#define HAL_MQTT_H
#include "main.h"
#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

#define REG_MODE                                    1
#define TRANSACTION_MODE                            2
#define FM_MODE                                     3
#define SPEAKER_CTRL_MODE                           4

extern void setup_wifi(String ssidPay, String passwordPay);
extern void mqttConnectionCheck();
extern void reconnect();
extern void callback(char *topic, byte *message, unsigned int length);
extern void mqttInit();
extern void jsonPrase(String payload);
extern void mqtt_publish(String topic, char payload[512]);

#endif