#ifndef HAL_GPIO_H
#define HAL_GPIO_H
#include "main.h"

#define RESET_BUTTON    18

extern void gpio_init(void);
//extern void IRAM_ATTR isrTrig();
extern uint8_t ioRead(uint8_t pin);

#endif