#ifndef HAL_WIFI_MANAGER_H
#define HAL_WIFI_MANAGER_H
#include "main.h"
#include <WiFiManager.h>
#include <EEPROM.h>

#define WIFI_NAME               "IppoPay"

extern void wifi_manager(void);

#endif